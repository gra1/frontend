class UsersClient
  def initialize; end

  def get_users
    url = 'https://mysterious-escarpment-9010.herokuapp.com/api/v1/users.json'
    JSON.parse(HTTP.get(url)).map { |obj| obj['name']}
  end

  def new_user(user_params)
    url = 'https://mysterious-escarpment-9010.herokuapp.com/api/v1/registrations'
    HTTP.post(url, form: user_params)
    get_users
  end
end
