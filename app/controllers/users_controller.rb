class UsersController < ApplicationController
  def index
    @users_name = UsersClient.new.get_users
  end

  def new; end

  def create
    UsersClient.new.new_user(params[:user])
    @users_name = UsersClient.new.get_users
    render :index
  end
end
